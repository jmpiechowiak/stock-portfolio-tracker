export const formatNumber = (n: number) => {
  let returnValue: string = '';
  const map: {[n: string]: string} = {
    '15': 'quad',
    '12': 'tril',
    '9': 'bil',
    '6': 'mil',
    '3': 'k'
  };

  Object.keys(map).some((key: string) => {
    const num = Number(key);
    if (n.toString().length > Number(key)) {
      returnValue = `${(n / Math.pow(10, num)).toFixed(2)} ${map[key]}`;
      return false;
    }
    return true;
  })

  return (returnValue || n.toFixed(2)).toString();
};