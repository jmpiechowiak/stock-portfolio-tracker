import {applyMiddleware, createStore, Store} from 'redux';
import createSagaMiddleware from 'redux-saga';

import { CompaniesReducer } from './redux/reducers/companies-reducer';
import { CompaniesSubstore } from './redux/types/companies-substore';
import { companiesSaga } from './redux/sagas/companies-saga';
import { NotificationsReducer } from './redux/reducers/notifications-reducer';
import { NotificationsSubstore } from './redux/types/notifications-substore';

export type State = {
  companies: CompaniesSubstore
  notifications: NotificationsSubstore
}

const rootReducer = (state: State, action: any) => ({
  companies: CompaniesReducer(state.companies, action),
  notifications: NotificationsReducer(state.notifications, action)
});

function* rootSaga() {
  yield companiesSaga();
}

const preloadedState = (state: Partial<State>) => rootReducer(state as State, {});

export const getStore = (state: Partial<State> = {}) => {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    rootReducer as any,
    preloadedState(state),
    applyMiddleware(sagaMiddleware)
  ) as Store<State>;
  sagaMiddleware.run(rootSaga);
  return store;
}