import React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { Container, Box } from '@mui/material';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';

import { Dashboard } from './containers/dashboard';
import { CompanyOverview } from './containers/company-overview';
import { NotificationsContainer } from './containers/notifications-container';

export const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(2),
}));

function App() {
  return (
    <Router>
      <div>
        <AppBar position="static">
          <Toolbar variant="dense">
            <Typography variant="h6" color="inherit" component="div">
              SDH Frontend Homework
            </Typography>
          </Toolbar>
        </AppBar>
        <Container fixed>
          <Box mt={1}>
            <Switch>
              <Route exact path='/home' component={Dashboard} />
              <Route path='/company/:symbol' component={CompanyOverview} />
              <Route path='/'>
                <Redirect to='/home' />
              </Route>
            </Switch>
          </Box>
        </Container>
        <NotificationsContainer />
      </div>
    </Router>
  );
}

export default App;
