import { formatNumber } from '../utils/number-utils';

test('Test number formating util', () => {
  const numbers: number[] = [
    10,
    2959,
    5939694,
    100000000042,
    5020594996964423
  ];

  expect(formatNumber(numbers[0])).toEqual('10.00');
  expect(formatNumber(numbers[1])).toEqual('2.96 k');
  expect(formatNumber(numbers[2])).toEqual('5.94 mil');
  expect(formatNumber(numbers[3])).toEqual('100.00 bil');
  expect(formatNumber(numbers[4])).toEqual('5.02 quad');
});