import { AxiosPromise } from 'axios';
import { runSaga } from 'redux-saga';
import { Saga } from '@redux-saga/types';

import { companiesFetchStockDataSaga, companiesFetchCompanyDetailsSaga } from '../redux/sagas/companies-saga';
import { AlphaVantageRest } from '../rest/alpha-vantage-rest';
import { companiesActions } from '../redux/reducers/companies-reducer';

test('Test fetching stock data saga', async () => {
  const bestMatches = [
    {
      "1. symbol": "TSCO.LON",
      "2. name": "Tesco PLC",
    },
    {
      "1. symbol": "TSCDF",
      "2. name": "Tesco plc",
    }
  ];

  jest.spyOn(AlphaVantageRest.axiosInstance, 'get').mockImplementationOnce(() => {
    return Promise.resolve({
      status: 200,
      data: { bestMatches }
    }) as AxiosPromise;
  });

  const dispatched: any[] = [];

  await runSaga(
    {
      dispatch: (action) => dispatched.push(action)
    },
    (companiesFetchStockDataSaga(AlphaVantageRest) as Saga),
    { keywords: 'tesco' }
  ).toPromise().then(() => {
    expect(dispatched[0].type).toEqual(companiesActions.COMPANIES_SET_REQUEST_IN_PROGRESS_FLAG);
    expect(dispatched[1].type).toEqual(companiesActions.COMPANIES_SET_DATA);
    expect(dispatched[1].data.stocksApiData)
      .toEqual(bestMatches.map((record: any) => ({ symbol: record['1. symbol'], name: record['2. name'] })));
    expect(dispatched[2].type).toEqual(companiesActions.COMPANIES_SET_REQUEST_IN_PROGRESS_FLAG);
  });
});

test('Test fetching company details saga', async () => {
  const data = {
    "Address": "1 NEW ORCHARD ROAD, ARMONK, NY, US",
    "MarketCapitalization": "128460587000",
    "Description": "Sample description"
  };

  jest.spyOn(AlphaVantageRest.axiosInstance, 'get').mockImplementationOnce(() => {
    return Promise.resolve({
      status: 200,
      data
    }) as AxiosPromise;
  });

  const dispatched: any[] = [];

  await runSaga(
    {
      dispatch: (action) => dispatched.push(action)
    },
    (companiesFetchCompanyDetailsSaga(AlphaVantageRest) as Saga),
    { symbol: 'IBM' }
  ).toPromise().then(() => {
    expect(dispatched[0].type).toEqual(companiesActions.COMPANIES_SET_REQUEST_IN_PROGRESS_FLAG);
    expect(dispatched[1].type).toEqual(companiesActions.COMPANIES_SAVE_DETAILS);
    expect(dispatched[1].details.address).toEqual(data.Address);
    expect(dispatched[1].details.description).toEqual(data.Description);
    expect(dispatched[1].details.capitalization).toEqual(data.MarketCapitalization);
    expect(dispatched[2].type).toEqual(companiesActions.COMPANIES_SET_REQUEST_IN_PROGRESS_FLAG);
  });
});