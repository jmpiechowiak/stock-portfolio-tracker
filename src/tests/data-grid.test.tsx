import React from 'react'
import { render, screen } from '@testing-library/react'

import { Column, Row, DataGrid } from '../components/data-grid';

test('Test if Data grid component renders data corretly', () => {
  const columns: Column[] = [
    { id: 'column1', header: 'First column' },
    { id: 'column2', header: 'Second column' }
  ];

  const rows: Row[] = [
    { column1: 'sample text' },
    { column2: '12345' }
  ];

  render(<DataGrid rows={rows} columns={columns} />);

  expect(screen.getAllByText('column', { exact: false }).length).toEqual(2);
  expect(screen.getByText('sample text')).toBeTruthy();
  expect(screen.getByText('12345')).toBeTruthy();
});

test('Test if Data grid component no data overlay', () => {
  const columns: Column[] = [
    { id: 'column1', header: 'First column' },
    { id: 'column2', header: 'Second column' }
  ];

  const rows: Row[] = [];

  render(<DataGrid rows={rows} columns={columns} noDataLabel='Testing no data label' />);

  expect(screen.getByText('Testing no data label')).toBeTruthy();
});