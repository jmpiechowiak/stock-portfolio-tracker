import React from 'react'
import { Provider } from 'react-redux';
import { render, screen } from '@testing-library/react'

import { NotificationsContainer } from '../containers/notifications-container';
import { getStore } from '../configureStore';
import { NotificationType } from "../redux/types/notifications-substore";

test('Testing empty notification container', () => {
  render(
    <Provider store={getStore()}>
      <NotificationsContainer />
    </Provider>
  );

  expect(screen.queryByTestId('notification-container')).toBeFalsy();
});

test('Testing warning notification container', () => {
  render(
    <Provider store={getStore({
      notifications: {
        notification: {
          type: NotificationType.warning,
          message: 'Sample message warning'
        }
      }
    })}>
      <NotificationsContainer />
    </Provider>
  );

  expect(screen.getByText('Sample message warning')).toBeTruthy();
});