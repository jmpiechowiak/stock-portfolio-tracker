import React from 'react'
import { render, screen, fireEvent } from '@testing-library/react'

import { Item, ItemList } from '../components/item-list';

test('Test Item List loading overlay', () => {
  render(<ItemList items={[]} loading={true} onItemSelect={() => {}} />);

  expect(screen.getByLabelText('loading indicator')).toBeTruthy();
});

test('Test Item List selecting items functionality', () => {
  const selectedItems: Item[] = [];
  const items: Item[] = [
    { label: 'item1', value: 'item1 value' },
    { label: 'item2', value: 'item2 value' }
  ];

  render(
    <ItemList
      items={items}
      loading={false}
      onItemSelect={(itemValue) => {
        selectedItems.push(itemValue)
      }}
    />
  );

  let index = 0;

  fireEvent(
    screen.getAllByLabelText('add')[index],
    new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
    })
  );

  expect(selectedItems[index]).toEqual(items[index].value);
  expect(selectedItems.length).toEqual(1);

  index = 1;

  fireEvent(
    screen.getAllByLabelText('add')[index],
    new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
    })
  );

  expect(selectedItems[index]).toEqual(items[index].value);
  expect(selectedItems.length).toEqual(2);
});