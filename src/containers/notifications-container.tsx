import React from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, {AlertProps} from '@mui/material/Alert';

import { NotificationsSubstore } from '../redux/types/notifications-substore';
import { NotificationsRemove } from '../redux/reducers/notifications-reducer';

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref,
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const mapStateToProps = (state: {
  notifications: NotificationsSubstore
}) => ({
  notification: state.notifications.notification
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  notificationRemove: () => dispatch(NotificationsRemove())
})

type Props = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
export class NotificationsContainerComponent extends React.Component<Props, any> {
  render() {
    const { type, message } = this.props.notification;
    return !(!type && !message) && (
      <Snackbar open={true} datatest-id='notification-container'>
        <Alert onClose={this.props.notificationRemove} severity={type} sx={{ width: '100%' }}>
          {message}
        </Alert>
      </Snackbar>
    );
  }
};

export const NotificationsContainer = connect(mapStateToProps, mapDispatchToProps)(NotificationsContainerComponent);