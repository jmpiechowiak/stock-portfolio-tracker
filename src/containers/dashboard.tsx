import React, { Component } from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Box from "@mui/material/Box";
import debounce from 'lodash/debounce';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import ListIcon from '@mui/icons-material/List';
import { Link } from 'react-router-dom';

import {
  CompaniesAddToCurrentStocks,
  CompaniesFetchStockData,
  CompaniesRemove,
  CompaniesSetData
} from '../redux/reducers/companies-reducer';
import { CompaniesSubstore, Stock } from '../redux/types/companies-substore';
import { SearchInput } from '../components/search-input';
import { ItemList } from '../components/item-list';
import { Column, DataGrid } from '../components/data-grid';
import { NotificationsAdd } from '../redux/reducers/notifications-reducer';
import { Notification, NotificationType } from '../redux/types/notifications-substore';
import { Item } from '../App';

const mapStateToProps = (state: {
  companies: CompaniesSubstore
}) => ({
  stocks: state.companies.stocksApiData,
  currentStocks: state.companies.currentStocks,
  noData: state.companies.noData,
  requestInProgress: state.companies.requestInProgress
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  companiesFetchStockData: (keywords: string) => dispatch(CompaniesFetchStockData(keywords)),
  companiesAddToCurrentStocks: (stock: Stock) => dispatch(CompaniesAddToCurrentStocks(stock)),
  companiesRemove: (symbol: string) => dispatch(CompaniesRemove(symbol)),
  clearData: () => dispatch(CompaniesSetData({ stocksApiData: [], noData: false  })),
  notificationsAdd: (notification: Notification) => dispatch(NotificationsAdd(notification))
})

type Props = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;
class DashboardComponent extends Component<Props, {}> {
  state = {
    searchQuery: ''
  };

  debouncedCompaniesFetchStockData = debounce((keywords: string) => this.props.companiesFetchStockData(keywords), 500);

  handleSearchQueryChange = (searchQuery: string) => this.setState({ searchQuery: searchQuery.trim() }, () => {
    searchQuery.trim() ? this.debouncedCompaniesFetchStockData(searchQuery) : this.props.clearData();
  });

  renderNoDataOverlay() {
    return (
      <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2, p: 1  }}>
        <Typography>
          No data found
        </Typography>
      </Box>
    );
  }

  renderSearchSection() {
    return (
      <Grid item xs={12} md={5}>
        <Item>
          <Typography sx={{ mb: 2 }} variant='h6'>Search for company stocks</Typography>
          <SearchInput
            id='search-companies'
            label='Company Name'
            placeholder='Example: Apple'
            fullWidth
            value={this.state.searchQuery}
            onChange={this.handleSearchQueryChange}
          />
          {this.props.noData ? this.renderNoDataOverlay() : (
            <ItemList
              items={
                this.props.stocks
                  .filter((stock: Stock) => !this.props.currentStocks.find((s: Stock) => stock.symbol === s.symbol))
                  .map((stock: Stock) =>
                    ({
                      label: `${stock.symbol} - ${stock.name}`,
                      value: stock
                    }))
              }
              loading={this.props.requestInProgress}
              onItemSelect={stock => this.props.companiesAddToCurrentStocks(stock)}
            />
          )}
        </Item>
      </Grid>
    );
  }

  renderSavedCompaniesSection() {
    const columns: Column[] = [
      { id: 'name', header: 'Company Name', sx: { width: '50%' }},
      { id: 'symbol', header: 'Symbol', sx: { width: '25%' }},
      { id: 'actions', header: 'Actions', sx: { width: '25%' }},
    ];

    return (
      <Grid item xs={12} md={7}>
        <Item>
          <Typography sx={{ mb: 2 }} variant='h6'>Your portfolio</Typography>
          <DataGrid
            columns={columns}
            rows={this.props.currentStocks.map(stock => ({
              ...stock,
              actions: (
                <>
                  <Link to={`/company/${stock.symbol}`}>
                    <IconButton
                      color="primary"
                      aria-label="show stock details"
                      component="span"
                    >
                      <ListIcon />
                    </IconButton>
                  </Link>
                  <IconButton
                    color="secondary"
                    aria-label="remove stock"
                    component="span"
                    onClick={() => {
                      this.props.companiesRemove(stock.symbol);
                      this.props.notificationsAdd({
                        type: NotificationType.success,
                        message: 'Stock data removed'
                      });
                    }}
                  >
                    <DeleteIcon />
                  </IconButton>
                </>
              )
            }))}
            noDataLabel='No company stocks saved yet'
          />
        </Item>
      </Grid>
    );
  }

  render() {
    return (
      <Grid container spacing={1}>
        {this.renderSearchSection()}
        {this.renderSavedCompaniesSection()}
      </Grid>
    );
  }
}

export const Dashboard = connect(mapStateToProps, mapDispatchToProps)(DashboardComponent);