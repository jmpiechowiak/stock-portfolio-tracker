import React, { Component } from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import {Redirect, RouteComponentProps, withRouter} from 'react-router-dom';
import Grid from '@mui/material/Grid';

import { CompaniesSubstore, Stock } from '../redux/types/companies-substore';
import { CompanyDetailsGridItem } from '../components/company-details-grid-item';
import { CompaniesFetchCompanyDetails } from '../redux/reducers/companies-reducer';

const mapStateToProps = (state: {
  companies: CompaniesSubstore
}) => ({
  currentStocks: state.companies.currentStocks,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  companiesFetchCompanyDetails: (symbol: string, callback?: () => void) => dispatch(CompaniesFetchCompanyDetails(symbol, callback))
})

type Props = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps> & RouteComponentProps;
class CompanyOverviewComponent extends Component<Props, {}> {
  state = {
    loading: true,
    unsavedStock: false
  };

  componentDidMount() {
    const symbol: string = (this.props.match.params as { symbol: string }).symbol;
    const currentStock: Stock | undefined = this.props.currentStocks.find(stock => stock.symbol === symbol);
    if (currentStock) {
      if (!currentStock.details)
        return this.props.companiesFetchCompanyDetails(symbol, () => this.setState({ loading: false }));
      this.setState({ loading: false });
    }
    else {
      this.setState({ unsavedStock: true });
    }
  }

  unsavedStockRedirect = () => <Redirect to='/home' />

  render() {
    return this.state.unsavedStock ? this.unsavedStockRedirect() : (
      <Grid container>
        <CompanyDetailsGridItem
          onBackButtonClick={() => this.props.history.push('/')}
          loading={this.state.loading}
          stockData={
            this.props.currentStocks.find(stock => stock.symbol === (this.props.match.params as { symbol: string }).symbol)
          }
        />
      </Grid>
    );
  }
}

export const CompanyOverview = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CompanyOverviewComponent)
);

