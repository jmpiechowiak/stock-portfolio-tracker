import axios, { AxiosResponse } from 'axios';

import { CompanyOverviewParams, SymbolSearchParams } from './types/alpha-vantage-rest-types';

const axiosConfig = {
  baseURL: 'https://www.alphavantage.co',
  params: { apikey: 'J8CNYSRVTJCD2I5W' }
};

export class AlphaVantageRest {
  static axiosInstance = axios.create(axiosConfig);
  static SYMBOL_SEARCH_PATH = '/query?function=SYMBOL_SEARCH';
  static COMPANY_OVERVIEW_PATH = '/query?function=OVERVIEW';

  static Search(params: SymbolSearchParams) {
    return this.axiosInstance.get(this.SYMBOL_SEARCH_PATH, { params })
      .then((response: AxiosResponse) => response.data);
  }

  static FetchCompanyDetails(params: CompanyOverviewParams) {
    return this.axiosInstance.get(this.COMPANY_OVERVIEW_PATH, { params })
      .then((response: AxiosResponse) => response.data);
  }
}