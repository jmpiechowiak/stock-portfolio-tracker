export interface SymbolSearchParams {
  keywords: string
}

export interface CompanyOverviewParams {
  symbol: string
}