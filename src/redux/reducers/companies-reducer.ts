import { CompaniesSettableData, CompaniesSubstore, Stock } from '../types/companies-substore';

export const companiesActions = {
  'COMPANIES_FETCH_STOCK_DATA': 'COMPANIES_FETCH_STOCK_DATA',
  'COMPANIES_FETCH_COMPANY_DETAILS': 'COMPANIES_FETCH_COMPANY_DETAILS',
  'COMPANIES_SET_DATA': 'COMPANIES_SET_DATA',
  'COMPANIES_SET_REQUEST_IN_PROGRESS_FLAG': 'COMPANIES_SET_REQUEST_IN_PROGRESS_FLAG',
  'COMPANIES_ADD_TO_CURRENT_STOCKS': 'COMPANIES_ADD_TO_CURRENT_STOCKS',
  'COMPANIES_SAVE_DETAILS': 'COMPANIES_SAVE_DETAILS',
  'COMPANIES_REMOVE': 'COMPANIES_REMOVE'
};

export const CompaniesFetchStockData = (keywords: string) => ({
  type: companiesActions.COMPANIES_FETCH_STOCK_DATA,
  keywords
});

export const CompaniesFetchCompanyDetails = (symbol: string, callback?: () => void) => ({
  type: companiesActions.COMPANIES_FETCH_COMPANY_DETAILS,
  symbol,
  callback
});

export const CompaniesSetData = (data: Partial<CompaniesSettableData>) => ({
  type: companiesActions.COMPANIES_SET_DATA,
  data
});

export const CompaniesSetRequestInProgressFlag = (requestInProgress: boolean) => ({
  type: companiesActions.COMPANIES_SET_REQUEST_IN_PROGRESS_FLAG,
  requestInProgress
});

export const CompaniesAddToCurrentStocks = (stock: Stock) => ({
  type: companiesActions.COMPANIES_ADD_TO_CURRENT_STOCKS,
  stock
});

export const CompaniesSaveDetails = (
  symbol: string,
  details: {
    address: string
    capitalization: string
    description: string
  }
) => ({
  type: companiesActions.COMPANIES_SAVE_DETAILS,
  symbol,
  details
});

export const CompaniesRemove = (symbol: string) => ({
  type: companiesActions.COMPANIES_REMOVE,
  symbol
});

export const CompaniesReducer = (
  state: CompaniesSubstore = {
    stocksApiData: [],
    currentStocks: [],
    requestInProgress: false,
    noData: false
  },
  action: any
): CompaniesSubstore => {
  let mutableCurrentStocks;
  switch (action.type) {
    case companiesActions.COMPANIES_SET_DATA:
      return { ...state, ...action.data };
    case companiesActions.COMPANIES_SET_REQUEST_IN_PROGRESS_FLAG:
      return { ...state, requestInProgress: action.requestInProgress };
    case companiesActions.COMPANIES_ADD_TO_CURRENT_STOCKS:
      return { ...state, currentStocks: [...state.currentStocks, action.stock] };
    case companiesActions.COMPANIES_SAVE_DETAILS:
      mutableCurrentStocks = [ ...state.currentStocks ];
      const stockIndex = mutableCurrentStocks.indexOf(
        mutableCurrentStocks.find(stock => stock.symbol === action.symbol)!
      );
      mutableCurrentStocks[stockIndex] = { ...mutableCurrentStocks[stockIndex], details: action.details };
      return { ...state, currentStocks: [ ...mutableCurrentStocks ] };
    case companiesActions.COMPANIES_REMOVE:
      mutableCurrentStocks = [ ...state.currentStocks ];
      mutableCurrentStocks.splice(
        mutableCurrentStocks.indexOf(
          mutableCurrentStocks.find((stock: Stock) => stock.symbol === action.symbol)!
        ), 1
      );
      return { ...state, currentStocks: [ ...mutableCurrentStocks ] };
    default:
      return state;
  }
};