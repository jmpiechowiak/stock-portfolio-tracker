import { NotificationsSubstore, Notification } from '../types/notifications-substore';

export const notificationsActions = {
  'NOTIFICATIONS_ADD': 'NOTIFICATIONS_ADD',
  'NOTIFICATIONS_REMOVE': 'NOTIFICATIONS_REMOVE'
}

export const NotificationsAdd = (notification: Notification) => ({
  type: notificationsActions.NOTIFICATIONS_ADD,
  notification
});

export const NotificationsRemove = () => ({
  type: notificationsActions.NOTIFICATIONS_REMOVE
});

export const NotificationsReducer = (
  state: NotificationsSubstore = {
    notification: {
      type: undefined,
      message: undefined
    }
  },
  action: any
): NotificationsSubstore => {
  switch (action.type) {
    case notificationsActions.NOTIFICATIONS_ADD:
      return { notification: action.notification };
    case notificationsActions.NOTIFICATIONS_REMOVE:
      return {
        notification: {
          type: undefined,
          message: undefined
        }
      };
    default:
      return state;
  }
};