export interface CompaniesSubstore extends CompaniesSettableData {
  currentStocks: Stock[]
  requestInProgress: boolean
}

export type Stock = {
  symbol: string
  name: string
  details?: {
    address: string
    capitalization: string
    description: string
  }
}

export interface CompaniesSettableData {
  stocksApiData: any[]
  noData: boolean
}