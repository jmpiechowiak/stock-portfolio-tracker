export enum NotificationType {
  error = 'error',
  warning = 'warning',
  info = 'info',
  success = 'success'
}

export type Notification = {
  message?: string
  type?: NotificationType
}

export interface NotificationsSubstore {
  notification: Notification
}