import { takeLatest, call, put } from 'redux-saga/effects'

import {
  companiesActions, CompaniesFetchCompanyDetails,
  CompaniesFetchStockData, CompaniesSaveDetails, CompaniesSetData,
  CompaniesSetRequestInProgressFlag
} from '../reducers/companies-reducer';
import { AlphaVantageRest } from '../../rest/alpha-vantage-rest';
import { NotificationsAdd } from '../reducers/notifications-reducer';
import { NotificationType } from '../types/notifications-substore';

export function* companiesSaga() {
  yield takeLatest(companiesActions.COMPANIES_FETCH_STOCK_DATA, companiesFetchStockDataSaga(AlphaVantageRest));
  yield takeLatest(companiesActions.COMPANIES_FETCH_COMPANY_DETAILS, companiesFetchCompanyDetailsSaga(AlphaVantageRest));
}

export const companiesFetchStockDataSaga = (service: any) => function* (action: ReturnType<typeof CompaniesFetchStockData>): any {
  try {
    yield put(CompaniesSetRequestInProgressFlag(true));
    const data = yield call(async() => await service.Search({ keywords: action.keywords }));
    const stocksApiData = data.bestMatches.map((record: any) => ({ symbol: record['1. symbol'], name: record['2. name'] })) || [];
    yield put(CompaniesSetData({
      stocksApiData,
      noData: stocksApiData.length === 0
    }));
  } catch (e) {
    yield put(NotificationsAdd({ type: NotificationType.error, message: 'Something went wrong while fetching stock data' }));
    console.log(e);
  }
  yield put(CompaniesSetRequestInProgressFlag(false));
}

export const companiesFetchCompanyDetailsSaga = (service: any) => function* (action: ReturnType<typeof CompaniesFetchCompanyDetails>): any {
  try {
    yield put(CompaniesSetRequestInProgressFlag(true));
    const data = yield call(async() => await service.FetchCompanyDetails({ symbol: action.symbol }));
    yield put(CompaniesSaveDetails(action.symbol, {
      address: data.Address,
      capitalization: data.MarketCapitalization,
      description: data.Description
    }));
  } catch (e) {
    yield put(NotificationsAdd({ type: NotificationType.error, message: 'Something went wrong while fetching company details' }));
    console.log(e);
  }
  yield put(CompaniesSetRequestInProgressFlag(false));
  action.callback && action.callback();
}