import React from 'react';
import InputAdornment from '@mui/material/InputAdornment';
import TextField from '@mui/material/TextField';
import SearchIcon from '@mui/icons-material/Search';

type PassedProps = {
  id: string
  label: string
  placeholder?: string
  fullWidth?: boolean
  value: string
  onChange: (value: string) => void
};

export const SearchInput = ({ id, label, placeholder, fullWidth, value, onChange }: PassedProps) => {
  return (
    <TextField
      id={id}
      label={label}
      placeholder={placeholder || 'Type here'}
      variant='outlined'
      fullWidth={fullWidth}
      value={value}
      InputProps={{
        startAdornment: (
          <InputAdornment position='start'>
            <SearchIcon />
          </InputAdornment>
        ),
      }}
      onChange={e => onChange(e.target.value)}
    />
  );
};