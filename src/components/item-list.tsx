import React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import IconButton from '@mui/material/IconButton';
import AddBoxIcon from '@mui/icons-material/AddBox';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

export type Item = { label: string, value: any };

type PassedProps = {
  items: Item[]
  loading: boolean
  onItemSelect: (itemValue: any) => void
};

export const ItemList = ({ items, loading, onItemSelect }: PassedProps) => {
  const renderLoadingIndicator = () => (
    <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2, p: 1 }}>
      <CircularProgress aria-label='loading indicator' />
    </Box>
  );

  const renderItem = (item: Item, key: number) => (
    <ListItem
      key={key}
      secondaryAction={
        <IconButton
          edge="end"
          aria-label="add"
          onClick={() => onItemSelect(item.value)}
        >
          <AddBoxIcon />
        </IconButton>
      }
    >
      <ListItemText
        id={`item-list-label-${key}`}
        primary={item.label}
      />
    </ListItem>
  );

  const renderList = () => items.length > 0 ? (
    <List sx={{
      mt: 2,
      width: '100%'
    }}>
      {items.map((item, index) => renderItem(item, index))}
    </List>
  ) : null;

  return loading ? renderLoadingIndicator() : renderList();
};