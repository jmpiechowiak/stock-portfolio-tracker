import React, { CSSProperties } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Typography from "@mui/material/Typography";

export type Row = { [id: string]: any }
export type Column = { id: string, header: string, align?: 'center' | 'right' | 'left', sx?: CSSProperties }

type PassedProps = {
  rows: Row[]
  columns: Column[]
  noDataLabel?: string
};

export const DataGrid = ({ rows, columns, noDataLabel }: PassedProps) => {
  const renderRow = (row: Row, key: number) => (
    <TableRow key={key}>
      {columns.map((col, index) => (
        <TableCell key={index} align={col.align || 'left'}>
          {row[col.id]}
        </TableCell>
      ))}
    </TableRow>
  );

  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            {columns.map((col, index) => <TableCell key={index} sx={col.sx}>{col.header}</TableCell>)}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row, index) => renderRow(row, index))}
        </TableBody>
      </Table>
      {rows.length === 0 && (
        <Typography sx={{ display: 'flex', justifyContent: 'center', m: 2, p: 1 }}>
          {noDataLabel ?? 'No data'}
        </Typography>
      )}
    </TableContainer>
  );
};