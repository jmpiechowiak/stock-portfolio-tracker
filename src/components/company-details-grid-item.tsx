import React from 'react';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';
import Typography from '@mui/material/Typography';

import { Stock } from '../redux/types/companies-substore';
import { formatNumber } from '../utils/number-utils';
import { Item } from '../App';

type PassedProps = {
  onBackButtonClick: () => void
  loading?: boolean
  stockData?: Stock
};

export const CompanyDetailsGridItem = ({ onBackButtonClick, loading, stockData }: PassedProps) => {
  const renderLoadingIndicator = () => (
    <Box sx={{ display: 'flex', justifyContent: 'center', m: 2, p: 1 }}>
      <CircularProgress />
    </Box>
  );

  const renderTextField = (key: string, value: string) => (
    <Box sx={{ mt: 2 }}>
      <span>
        <Typography sx={{ fontWeight: 'bold' }}>{key}</Typography>
        <Typography>{value}</Typography>
      </span>
    </Box>
  );

  return (
    <Grid item xs={12}>
      <Item>
        {loading ? renderLoadingIndicator() : (
          <>
            <Button variant="contained" onClick={onBackButtonClick}>Go Back</Button>
            <Typography sx={{ mt: 3 }} variant='h5'>{stockData!.name}</Typography>
            {stockData!.details!.address && renderTextField('Address', stockData!.details!.address)}
            {stockData!.details!.capitalization && renderTextField('Market Capitalization', formatNumber(Number(stockData!.details!.capitalization)))}
            {stockData!.details!.description && <Typography sx={{mt: 2}}>{stockData!.details!.description}</Typography>}
          </>
        )}
      </Item>
    </Grid>
  );
};